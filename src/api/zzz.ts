import {
  _getZZZRoleInfo,
  _getZZZWeaponInfo,
  _getZZZRelationInfo,
  _getZZZSuitInfo,
  _getZZZBangbooInfo
} from '@/backEnd';
// 原神相关接口


// 分页查询角色信息
export const getZZZRoleInfo = (params: any) => {
  return _getZZZRoleInfo(params)
}
// 获取关系列表
export const getZZZRelationInfo = (type = "") => {
  return _getZZZRelationInfo(type)
}
// 分页查询武器信息
export const getZZZWeaponInfo = (params: any) => {
  return _getZZZWeaponInfo(params)
}
// 分页查询套装信息
export const getZZZSuitInfo = (params: any) => {
  return _getZZZSuitInfo(params)
}
// 分页获取邦布信息
export const getZZZBangbooInfo = (params: any) => {
  return _getZZZBangbooInfo(params)
}