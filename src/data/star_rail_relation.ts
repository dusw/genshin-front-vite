export const starRailRelation: any[] = [
  {
    "id": 0,
    "element_type": "物理",
    "weapon_type": "毁灭"
  },
  {
    "id": 1,
    "element_type": "火",
    "weapon_type": "智识"
  },
  {
    "id": 2,
    "element_type": "风",
    "weapon_type": "巡猎"
  },
  {
    "id": 3,
    "element_type": "冰",
    "weapon_type": "存护"
  },
  {
    "id": 4,
    "element_type": "雷",
    "weapon_type": "丰饶"
  },
  {
    "id": 5,
    "element_type": "虚数",
    "weapon_type": "同谐"
  },
  {
    "id": 6,
    "element_type": "量子",
    "weapon_type": "虚无"
  },
  {
    "id": 7,
    "element_type": null,
    "weapon_type": "记忆"
  }
]