
export const genshinSelectBook: any[] = [
  {
    "id": "0",
    "name": "「自由」",
    "icon_url": "https://uploadstatic.mihoyo.com/ys-obc/2020/07/01/80410800/127e8ff10e65542d119a1b895e091b46_8324184099114143317.png"
  },
  {
    "id": "1",
    "name": "「抗争」",
    "icon_url": "https://uploadstatic.mihoyo.com/ys-obc/2020/07/01/80410800/fc803239634f9035664b818f08e42f08_6255025949454286240.png"
  },
  {
    "id": "2",
    "name": "「诗文」",
    "icon_url": "https://uploadstatic.mihoyo.com/ys-obc/2020/07/01/80410800/86fd228ce3aa9b5f4541b34c2ac6c3b4_2768286836747901422.png"
  },
  {
    "id": "3",
    "name": "「繁荣」",
    "icon_url": "https://uploadstatic.mihoyo.com/ys-obc/2020/07/01/80410800/08fd7ca78f0a3d76943dcabb8c4c984d_4225296401185479449.png"
  },
  {
    "id": "4",
    "name": "「勤劳」",
    "icon_url": "https://uploadstatic.mihoyo.com/ys-obc/2020/07/01/80410800/7bcdfc964568483041339346b20ac870_8802895355053984746.png"
  },
  {
    "id": "5",
    "name": "「黄金」",
    "icon_url": "https://uploadstatic.mihoyo.com/ys-obc/2020/07/01/80410800/5c31d23006a125fe771fe295a92ea661_8115507080777026141.png"
  },
  {
    "id": "6",
    "name": "「浮世」",
    "icon_url": "https://uploadstatic.mihoyo.com/ys-obc/2021/07/21/15568211/8c7290c464927a8f3a5c91a19bc55e4c_2652835632088181801.png"
  },
  {
    "id": "7",
    "name": "「风雅」",
    "icon_url": "https://uploadstatic.mihoyo.com/ys-obc/2021/07/21/15568211/994b1453bcc87664a6815496715bd569_4763952261692427253.png"
  },
  {
    "id": "8",
    "name": "「天光」",
    "icon_url": "https://uploadstatic.mihoyo.com/ys-obc/2021/07/21/15568211/9101bb051b761d15db3b811e3c11f35e_1337682300909585584.png"
  },
  {
    "id": "9",
    "name": "「笃行」",
    "icon_url": "https://uploadstatic.mihoyo.com/ys-obc/2022/08/20/4820086/1b711ad34080e0d1bccc4b06ff167289_8900037121081736299.png"
  },
  {
    "id": "10",
    "name": "「巧思」",
    "icon_url": "https://uploadstatic.mihoyo.com/ys-obc/2022/08/20/4820086/ad09c0f8b089b2e7b8c54abe4aa149db_3983505189235785710.png"
  },
  {
    "id": "11",
    "name": "「诤言」",
    "icon_url": "https://uploadstatic.mihoyo.com/ys-obc/2022/08/20/4820086/d62bd81605203d46b57ad7bc71357f1e_8614656753128774889.png"
  },
  {
    "id": "12",
    "name": "「公平」",
    "icon_url": "https://act-upload.mihoyo.com/ys-obc/2023/08/13/183046623/26bdd48703ac69b8c9a3839f9f1e8683_9091580550242112679.png"
  },
  {
    "id": "13",
    "name": "「正义」",
    "icon_url": "https://act-upload.mihoyo.com/ys-obc/2023/08/13/183046623/b2a839f0a7793ad8ff3cfbc5a030b8bb_4503835064374070869.png"
  },
  {
    "id": "14",
    "name": "「秩序」",
    "icon_url": "https://act-upload.mihoyo.com/ys-obc/2023/08/13/183046623/9c832ec97dd940eb2ab5a560c167fb5a_1575127459787759342.png"
  },
  {
    "id": "15",
    "name": "「纷争」",
    "icon_url": "https://act-upload.mihoyo.com/wiki-user-upload/2024/08/23/183046623/c31db9554b91303b2b112bf17e227515_6972566881817243529.png"
  },
  {
    "id": "16",
    "name": "「焚燔」",
    "icon_url": "https://act-upload.mihoyo.com/wiki-user-upload/2024/08/23/183046623/959088ee0083cd06e6bccada22ae5f67_977446571485181933.png"
  },
  {
    "id": "17",
    "name": "「角逐」",
    "icon_url": "https://act-upload.mihoyo.com/wiki-user-upload/2024/08/23/183046623/bc8fd490403dc8fc9a2011113e91cf18_4529717785541771759.png"
  }
]

export const genshinSelectItem: any[] = [
  {
    "id": "0",
    "name": "高塔孤王",
    "icon_url": "https://uploadstatic.mihoyo.com/ys-obc/2021/08/22/6381993/4c7f5f2d68758f598cdf8bebc3de7c25_1938045084620419994.png"
  },
  {
    "id": "1",
    "name": "凛风奔狼",
    "icon_url": "https://uploadstatic.mihoyo.com/ys-obc/2021/08/22/6381993/ed6eb531c6f385d6133b0c30b4f47ce2_6225207976407893285.png"
  },
  {
    "id": "2",
    "name": "狮牙斗士",
    "icon_url": "https://uploadstatic.mihoyo.com/ys-obc/2021/08/22/6381993/a71e216aaa6297056e887e583c6df932_602783919022092895.png"
  },
  {
    "id": "3",
    "name": "孤云寒林",
    "icon_url": "https://uploadstatic.mihoyo.com/ys-obc/2021/08/22/6381993/a86fb2d5384fce5d96e7abbaca63e885_1957888893081972562.png"
  },
  {
    "id": "4",
    "name": "雾海云间",
    "icon_url": "https://uploadstatic.mihoyo.com/ys-obc/2021/08/22/6381993/36f61a67d9edcb783ded72744e5a0e8d_1717976347652230886.png"
  },
  {
    "id": "5",
    "name": "漆黑陨铁",
    "icon_url": "https://uploadstatic.mihoyo.com/ys-obc/2021/08/22/6381993/0f52ddaaa4cb5b199ac18c6ede6488df_4087850740504825348.png"
  },
  {
    "id": "6",
    "name": "远海夷地",
    "icon_url": "https://uploadstatic.mihoyo.com/ys-obc/2021/07/23/15568211/ba86b38f692b675b94a3af01c3098b8e_3129009942985483051.png"
  },
  {
    "id": "7",
    "name": "鸣神御灵",
    "icon_url": "https://uploadstatic.mihoyo.com/ys-obc/2021/07/23/15568211/d2320d5d0e2d2efe207839567387cc2a_5541601613916812653.png"
  },
  {
    "id": "8",
    "name": "今昔剧画",
    "icon_url": "https://uploadstatic.mihoyo.com/ys-obc/2021/07/23/15568211/f79d76eb9c5e434a620cd88069c736bf_217235698444563929.png"
  },
  {
    "id": "9",
    "name": "烈日威权",
    "icon_url": "https://uploadstatic.mihoyo.com/ys-obc/2022/08/20/4820086/26b905c578907b9b13add0d186e2c1ad_6102952905987485129.png"
  },
  {
    "id": "10",
    "name": "绿洲花园",
    "icon_url": "https://uploadstatic.mihoyo.com/ys-obc/2022/08/20/4820086/43c303962c12113250be3ee1f6a41f4e_8390543528822267743.png"
  },
  {
    "id": "11",
    "name": "谧林涓露",
    "icon_url": "https://uploadstatic.mihoyo.com/ys-obc/2022/08/20/4820086/55416fff3427034075a4201d4fb4abbe_682407200285386977.png"
  },
  {
    "id": "12",
    "name": "无垢之海",
    "icon_url": "https://act-upload.mihoyo.com/ys-obc/2023/08/13/183046623/8e30583b8749060c0a47fa9809fc334b_2652530486444795558.png"
  },
  {
    "id": "13",
    "name": "悠古弦音",
    "icon_url": "https://act-upload.mihoyo.com/ys-obc/2023/08/15/183046623/e340de45b1e7e341a765cc2ad0ba9f98_3406224953553810220.png"
  },
  {
    "id": "14",
    "name": "纯圣露滴",
    "icon_url": "https://act-upload.mihoyo.com/ys-obc/2023/08/15/183046623/c134add56fc2fc73353b9d9bda587729_6189837891370128908.png"
  },
  {
    "id": "15",
    "name": "谵妄圣主",
    "icon_url": "https://act-upload.mihoyo.com/wiki-user-upload/2024/08/23/288180982/9cc7a1c5d4f9d8c1f39ed24e57727cf7_7387773953431248618.png"
  },
  {
    "id": "16",
    "name": "贡祭炽心",
    "icon_url": "https://act-upload.mihoyo.com/wiki-user-upload/2024/08/23/288180982/cc2a0eafc4d8c0fa629c998d76e2e7b4_6349425710746963891.png"
  },
  {
    "id": "17",
    "name": "神合秘烟",
    "icon_url": "https://act-upload.mihoyo.com/wiki-user-upload/2024/08/23/288180982/49db0551113b980c735c2082e8b0f086_4927741919306693608.png"
  },
]