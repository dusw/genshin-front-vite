export const zzzRelation: any[] = [
  {
    "id": -1,
    "element_type": "全部",
    "weapon_type": "全部",
    "area_type": "全部",
    "book_type": "全部",
    "item_type": "全部",
    "week_name": null
  },
  {
    "id": 0,
    "element_type": "电",
    "weapon_type": "异常",
    "area_type": "狡兔屋",
    "book_type": null,
    "item_type": null,
    "week_name": "未知"
  },
  {
    "id": 1,
    "element_type": "火",
    "weapon_type": "强攻",
    "area_type": "刑侦特勤组",
    "book_type": null,
    "item_type": null,
    "week_name": "周一、周四"
  },
  {
    "id": 2,
    "element_type": "冰",
    "weapon_type": "防护",
    "area_type": "H.S.O.S.6",
    "book_type": null,
    "item_type": null,
    "week_name": "周二、周五"
  },
  {
    "id": 3,
    "element_type": "以太",
    "weapon_type": "击破",
    "area_type": "白祇重工",
    "book_type": null,
    "item_type": null,
    "week_name": "周三、周六"
  },
  {
    "id": 4,
    "element_type": "物理",
    "weapon_type": "支援",
    "area_type": "维多利亚家政",
    "book_type": null,
    "item_type": null,
    "week_name": null
  },
  {
    "id": 5,
    "element_type": null,
    "weapon_type": null,
    "area_type": "卡吕冬之子",
    "book_type": null,
    "item_type": null,
    "week_name": null
  },
  {
    "id": 6,
    "element_type": null,
    "weapon_type": null,
    "area_type": "奥波勒斯小队",
    "book_type": null,
    "item_type": null,
    "week_name": null
  },
  {
    "id": 7,
    "element_type": null,
    "weapon_type": null,
    "area_type": "天琴座",
    "book_type": null,
    "item_type": null,
    "week_name": null
  }
]