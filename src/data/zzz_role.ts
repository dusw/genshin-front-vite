export const zzzRole: any[] = [
  {
    "id": 1,
    "name": "11号",
    "star": 5,
    "element": 1,
    "weapon": 1,
    "book": 1,
    "area": 6,
    "mhy_url": "73",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/25/76099754/41cac9b7c43299248f50ef69a8d4d6fe_9121267123322404.png"
  },
  {
    "id": 2,
    "name": "艾莲",
    "star": 5,
    "element": 2,
    "weapon": 1,
    "book": 2,
    "area": 4,
    "mhy_url": "317",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/28/160132993/b7f945843942bc32064f240317ae7beb_617780162075671896.png"
  },
  {
    "id": 3,
    "name": "柏妮思",
    "star": 5,
    "element": 1,
    "weapon": 0,
    "book": 1,
    "area": 5,
    "mhy_url": "840",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/10/08/76099754/c00700f00787444df5c7de8b09d81766_3155659045192705400.png"
  },
  {
    "id": 4,
    "name": "格莉丝",
    "star": 5,
    "element": 0,
    "weapon": 0,
    "book": 0,
    "area": 3,
    "mhy_url": "150",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/21/160824341/e16ac4c5e1f06ba44b1fd48539916be3_3243900714756554770.png"
  },
  {
    "id": 5,
    "name": "简",
    "star": 5,
    "element": 4,
    "weapon": 0,
    "book": 4,
    "area": 1,
    "mhy_url": "759",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/08/16/15559334/671b9f8d9227c19355e2eceb9f23ef28_7055750575833971395.png"
  },
  {
    "id": 6,
    "name": "凯撒",
    "star": 5,
    "element": 4,
    "weapon": 2,
    "book": 4,
    "area": 5,
    "mhy_url": "801",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/09/17/82500917/e9717d0ff840e4457554e5d856a4b0d8_417496063639684958.png"
  },
  {
    "id": 7,
    "name": "凯撒",
    "star": 5,
    "element": 4,
    "weapon": 2,
    "book": 4,
    "area": 5,
    "mhy_url": "801",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/10/08/76099754/45788df6a0a15baf27d33f11a93e8c04_1410824794677572273.png"
  },
  {
    "id": 8,
    "name": "珂蕾妲",
    "star": 5,
    "element": 1,
    "weapon": 3,
    "book": 1,
    "area": 3,
    "mhy_url": "318",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/27/82232455/e49ffec1188f11ac70f2fd7e500fbd2c_5771997888796504931.png"
  },
  {
    "id": 9,
    "name": "莱卡恩",
    "star": 5,
    "element": 2,
    "weapon": 3,
    "book": 2,
    "area": 4,
    "mhy_url": "65",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/25/76099754/66038988b3920216f826555eb31ec7d3_1374786350678823683.png"
  },
  {
    "id": 10,
    "name": "莱特",
    "star": 5,
    "element": 1,
    "weapon": 4,
    "book": 1,
    "area": 5,
    "mhy_url": "950",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/11/21/76099754/97be522aa2bce70b13167df8b42e176f_6973279715938714866.png"
  },
  {
    "id": 11,
    "name": "丽娜",
    "star": 5,
    "element": 0,
    "weapon": 4,
    "book": 0,
    "area": 4,
    "mhy_url": "316",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/29/141353238/9a978a559ee6822481b7d4143170affe_464856154643405893.png"
  },
  {
    "id": 12,
    "name": "柳",
    "star": 5,
    "element": 0,
    "weapon": 0,
    "book": 0,
    "area": 2,
    "mhy_url": "916",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/10/29/76099754/dc3af8ec666a63e03d49b88aac5bb044_8292939597769078774.png"
  },
  {
    "id": 13,
    "name": "猫又",
    "star": 5,
    "element": 4,
    "weapon": 1,
    "book": 4,
    "area": 0,
    "mhy_url": "378",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/31/79697369/e926c03ff8172cfc3a010cfcbe76d238_6884319312264072107.png"
  },
  {
    "id": 14,
    "name": "青衣",
    "star": 5,
    "element": 0,
    "weapon": 3,
    "book": 0,
    "area": 1,
    "mhy_url": "680",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/08/04/141353238/77d70d7c49d4785dbb8d98438219cde7_7990459774903021190.png"
  },
  {
    "id": 15,
    "name": "雅",
    "star": 5,
    "element": 2,
    "weapon": 0,
    "book": 2,
    "area": 2,
    "mhy_url": "996",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/12/09/82500917/8facd72e9178da44d0708bfc4e1d5ade_1655620996938124425.jpg"
  },
  {
    "id": 16,
    "name": "耀嘉音",
    "star": 5,
    "element": 3,
    "weapon": 4,
    "book": 3,
    "area": 7,
    "mhy_url": "-",
    "icon_url": "https://bbs-static.miyoushe.com/static/2024/12/16/9240943125e9ce07683427478c30c699_3310414641678326826.png"
  },
  {
    "id": 17,
    "name": "伊芙琳",
    "star": 5,
    "element": 1,
    "weapon": 1,
    "book": 1,
    "area": 7,
    "mhy_url": "-",
    "icon_url": "https://upload-bbs.miyoushe.com/upload/2024/12/22/247584033/bb244c564c17d8b64cf1538cbc3ea6bd_8159780325823325145.jpg"
  },
  {
    "id": 18,
    "name": "悠真",
    "star": 5,
    "element": 0,
    "weapon": 1,
    "book": 0,
    "area": 2,
    "mhy_url": "997",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/12/09/82500917/7477bdb2d366fd83b30671812c08e2eb_7866771727764017477.jpg"
  },
  {
    "id": 19,
    "name": "朱鸢",
    "star": 5,
    "element": 3,
    "weapon": 1,
    "book": 3,
    "area": 1,
    "mhy_url": "634",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/07/06/15559334/1c64debafd7d934f8361b36026426940_897366969958613262.png"
  },
  {
    "id": 20,
    "name": "安比",
    "star": 4,
    "element": 0,
    "weapon": 3,
    "book": 0,
    "area": 0,
    "mhy_url": "379",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/06/01/76099754/eb28176da1bfd52ecd00c96c6641fe0d_863124956510266341.png"
  },
  {
    "id": 21,
    "name": "安东",
    "star": 4,
    "element": 0,
    "weapon": 1,
    "book": 0,
    "area": 3,
    "mhy_url": "147",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/25/76099754/b323dfb93362d929e64562c004cdd0e1_79025806971412847.png"
  },
  {
    "id": 22,
    "name": "本",
    "star": 4,
    "element": 1,
    "weapon": 2,
    "book": 1,
    "area": 3,
    "mhy_url": "158",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/25/76099754/63ea02398b416017922f38becab2cb20_5905737788415760146.png"
  },
  {
    "id": 23,
    "name": "比利",
    "star": 4,
    "element": 4,
    "weapon": 1,
    "book": 4,
    "area": 0,
    "mhy_url": "371",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/31/76099754/149feef57a5210d07d646ff3dec3581f_214202686184867036.png"
  },
  {
    "id": 24,
    "name": "苍角",
    "star": 4,
    "element": 2,
    "weapon": 4,
    "book": 2,
    "area": 2,
    "mhy_url": "227",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/27/339584194/5dbe373f3e54d4e3033e4dc7567fe9f8_2360629584131406565.png"
  },
  {
    "id": 25,
    "name": "可琳",
    "star": 4,
    "element": 4,
    "weapon": 1,
    "book": 4,
    "area": 4,
    "mhy_url": "179",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/28/110289248/90abac5b57bd5b48fa79e9efb5254360_7680652488792868823.png"
  },
  {
    "id": 26,
    "name": "露西",
    "star": 4,
    "element": 1,
    "weapon": 4,
    "book": 1,
    "area": 5,
    "mhy_url": "493",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/07/01/34600917/3fa01447d481351416ef664e21894c29_801262856017991825.png"
  },
  {
    "id": 27,
    "name": "妮可",
    "star": 4,
    "element": 3,
    "weapon": 4,
    "book": 3,
    "area": 0,
    "mhy_url": "80",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/25/76099754/1cb64706be3eca5400e7cc02b8a82e60_3042993825218318926.png"
  },
  {
    "id": 28,
    "name": "派派",
    "star": 4,
    "element": 4,
    "weapon": 0,
    "book": 4,
    "area": 5,
    "mhy_url": "485",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/07/01/76099754/90668b1414a002827f47c02feccbdf3e_8975374000446600175.png"
  },
  {
    "id": 29,
    "name": "赛斯",
    "star": 4,
    "element": 0,
    "weapon": 2,
    "book": 0,
    "area": 1,
    "mhy_url": "758",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/08/26/82500917/a99297e48c27c1c09ccee2f8cf3d4745_8604815710532543749.png"
  }
]