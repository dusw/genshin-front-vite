import { Option } from './interface';
import { getZZZRelationInfo } from "@/api/zzz"
import { zzzSuit } from "@/data/zzz_suit"
import { useMessage } from "naive-ui";
import { storage } from "./storage"

const message = useMessage();
const zh = ['一', '二', '三', '四', '五']

export const queryZZZRelation = async () => {
  let zzzRelation = storage.get('zzzRelation')
  if (zzzRelation) {
    return zzzRelation
  }
  let { code, data, msg } = await getZZZRelationInfo() as any
  if (code != 200) {
    message.error(msg)
  }
  const all: Option = {
    label: "所有类型",
    value: -2
  }
  let area: Option[] = [{ ...all, label: '所有阵营' }],
    element: Option[] = [{ ...all, label: '所有元素' }],
    weapon: Option[] = [{ ...all, label: '所有音擎类型' }],
    suit: Option[] = [{ ...all, label: '所有驱动盘套装' }],
    week: Option[] = [{ ...all, label: '所有星期' }],
    star: Option[] = [{ ...all, label: '所有星级' }];
  data.map((e: any) => {
    if (e.area_type != null) {
      area.push({
        label: e.area_type,
        value: e.id
      })
    }
    if (e.element_type != null) {
      element.push({
        label: e.element_type,
        value: e.id
      })
    }
    if (e.weapon_type != null) {
      weapon.push({
        label: e.weapon_type,
        value: e.id
      })
    }
    if (e.week_name != null) {
      week.push({
        label: e.week_name,
        value: e.id
      })
    }
  })
  zzzSuit.forEach((e: any, index: number) => {
    suit.push({
      label: e.name,
      value: e.id
    })
  });
  for (let i = 1; i < 6; ++i) {
    star.push({
      label: `${zh[i - 1]}星`,
      value: i
    })
  }
  const relation = {
    area, element, weapon, week, star, suit
  } as any
  storage.set('zzzRelation', relation)
  return relation
}