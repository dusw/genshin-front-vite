export const wavesRole: any[] = [
  {
    "id": 1,
    "name": "安可",
    "star": 5,
    "element": 3,
    "weapon": 4,
    "item": 2,
    "url": "1242296163679776768",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/60608a0a87b84cdcb195305970f007c620240516.png"
  },
  {
    "id": 2,
    "name": "椿",
    "star": 5,
    "element": 5,
    "weapon": 2,
    "item": 4,
    "url": "1302065018502307840",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/574dd26f1d3e4518a59860d32dfd3f2620240912.png"
  },
  {
    "id": 3,
    "name": "菲比",
    "star": 5,
    "element": null,
    "weapon": null,
    "item": null,
    "url": "1309523456688947200",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/46eb35b315044cc49d577b8b03c4d58a20241122.png"
  },
  {
    "id": 4,
    "name": "忌炎",
    "star": 5,
    "element": 0,
    "weapon": 0,
    "item": 1,
    "url": "1240073643014045696",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/0a046ba41d274b46850d710fe0e068c420240516.png"
  },
  {
    "id": 5,
    "name": "鉴心",
    "star": 5,
    "element": 0,
    "weapon": 1,
    "item": 0,
    "url": "1242295527595823104",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/ef1200b85e2141cda8e1d0414d76958d20240516.png"
  },
  {
    "id": 6,
    "name": "今汐",
    "star": 5,
    "element": 4,
    "weapon": 0,
    "item": 1,
    "url": "1249040336606580736",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/c029364d7d7f4587a088a60c6b763d8b20240701.png"
  },
  {
    "id": 7,
    "name": "卡卡罗",
    "star": 5,
    "element": 1,
    "weapon": 0,
    "item": 1,
    "url": "1242295483584421888",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/0f38553c30c148a89d417463457dd71520240516.png"
  },
  {
    "id": 8,
    "name": "珂莱塔",
    "star": 5,
    "element": 2,
    "weapon": 3,
    "item": 5,
    "url": "1309607893299945472",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/0a4ef56e74564a1f986e6743c14dc29320241124.png"
  },
  {
    "id": 9,
    "name": "凌阳",
    "star": 5,
    "element": 2,
    "weapon": 1,
    "item": 0,
    "url": "1242296125975633920",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/d5644b750f094fbaaee0b14f4fe9bc5520240516.png"
  },
  {
    "id": 10,
    "name": "洛可可",
    "star": 5,
    "element": 5,
    "weapon": 1,
    "item": 5,
    "url": "1309606674234638336",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/223abbd525d34a2ebabb042460e9184c20241125.png"
  },
  {
    "id": 11,
    "name": "漂泊者-男-湮灭",
    "star": 5,
    "element": 5,
    "weapon": 2,
    "item": 4,
    "url": "1242115703531454464",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/4a17be9c00854f9ca85f4b460b32653f20240527.png"
  },
  {
    "id": 12,
    "name": "漂泊者-男-衍射",
    "star": 5,
    "element": 4,
    "weapon": 2,
    "item": 4,
    "url": "1239724074505539584",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/987ea840e0034091b409e4a5b7d320d120240527.png"
  },
  {
    "id": 13,
    "name": "漂泊者-女-湮灭",
    "star": 5,
    "element": 5,
    "weapon": 2,
    "item": 4,
    "url": "1242103196649603072",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/b406ac3876794ec6bbfe538fbe40ae4220240527.png"
  },
  {
    "id": 14,
    "name": "漂泊者-女-衍射",
    "star": 5,
    "element": 4,
    "weapon": 2,
    "item": 4,
    "url": "1242294789908504576",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/b44dad638ce6498284fb5b569c0a2ed720240527.png"
  },
  {
    "id": 15,
    "name": "守岸人",
    "star": 5,
    "element": 4,
    "weapon": 4,
    "item": 2,
    "url": "1286814658335739904",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/f659feab4e494c31acf68246a172324420240811.png"
  },
  {
    "id": 16,
    "name": "维里奈",
    "star": 5,
    "element": 4,
    "weapon": 4,
    "item": 2,
    "url": "1242295554161025024",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/76b353a5c6484f56b6136ebf66f884c020240516.png"
  },
  {
    "id": 17,
    "name": "相里要",
    "star": 5,
    "element": 1,
    "weapon": 1,
    "item": 0,
    "url": "1272626941409398784",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/c575cb08352940e690305ec0006d200e20240811.png"
  },
  {
    "id": 18,
    "name": "吟霖",
    "star": 5,
    "element": 1,
    "weapon": 4,
    "item": 2,
    "url": "1239319812692709376",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/53a5e352ef56496eb0a8a7d6eb43327420240606.png"
  },
  {
    "id": 19,
    "name": "长离",
    "star": 5,
    "element": 3,
    "weapon": 2,
    "item": 4,
    "url": "1262882649069621248",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/aeff180b9bd4413ead67e9222101594120240811.png"
  },
  {
    "id": 20,
    "name": "折枝",
    "star": 5,
    "element": 2,
    "weapon": 4,
    "item": 2,
    "url": "1269802618305703936",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/91471558ec4c41eb86eb6ae151ac56b620240811.png"
  },
  {
    "id": 21,
    "name": "白芷",
    "star": 4,
    "element": 2,
    "weapon": 4,
    "item": 2,
    "url": "1233429754977792000",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/958496ed75854bd98449b419a2c7842d20240516.png"
  },
  {
    "id": 22,
    "name": "布兰特",
    "star": 4,
    "element": null,
    "weapon": null,
    "item": null,
    "url": "1309566040441675776",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/e6815fe9d28b45c0b9be0af7a902a3d320241122.png"
  },
  {
    "id": 23,
    "name": "炽霞",
    "star": 4,
    "element": 3,
    "weapon": 3,
    "item": 3,
    "url": "1240039812638883840",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/3e098f94b9314c979200fd342e0bc89520240516.png"
  },
  {
    "id": 24,
    "name": "丹瑾",
    "star": 4,
    "element": 5,
    "weapon": 2,
    "item": 4,
    "url": "1233430505140629504",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/05d2882b5bbc4d50b7e9a54ca5a9c34420240516.png"
  },
  {
    "id": 25,
    "name": "灯灯",
    "star": 4,
    "element": 1,
    "weapon": 0,
    "item": 0,
    "url": "1312376371368607744",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/8e451789792d4b80963a4af5e562a39e20240913.png"
  },
  {
    "id": 26,
    "name": "莫特斐",
    "star": 4,
    "element": 3,
    "weapon": 3,
    "item": 3,
    "url": "1240157802556833792",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/7144c592e83f4bc695b9eea3065a922b20240516.png"
  },
  {
    "id": 27,
    "name": "秋水",
    "star": 4,
    "element": 0,
    "weapon": 3,
    "item": 3,
    "url": "1233210783276019712",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/154338ab1ce24c07ad939913e62aed3320240516.png"
  },
  {
    "id": 28,
    "name": "散华",
    "star": 4,
    "element": 2,
    "weapon": 2,
    "item": 4,
    "url": "1233492733107564544",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/42e5f2298c544b0f88dfa248914f02d820240516.png"
  },
  {
    "id": 29,
    "name": "桃祈",
    "star": 4,
    "element": 5,
    "weapon": 0,
    "item": 1,
    "url": "1240051046261592064",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/1bf059fd27d1467786469d632d698d9a20240516.png"
  },
  {
    "id": 30,
    "name": "秧秧",
    "star": 4,
    "element": 0,
    "weapon": 2,
    "item": 4,
    "url": "1233436648562032640",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/266bbd25a203481bbf02fd4cf38f38ec20240516.png"
  },
  {
    "id": 31,
    "name": "釉瑚",
    "star": 4,
    "element": 2,
    "weapon": 1,
    "item": 0,
    "url": "1271918674232614912",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/6245a5688a724a9287b9f4648d2237ff20240811.png"
  },
  {
    "id": 32,
    "name": "渊武",
    "star": 4,
    "element": 1,
    "weapon": 1,
    "item": 0,
    "url": "1239990507047161856",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/318e1f2e17a4416eb7213e62d7f153ae20240516.png"
  },
  {
    "id": 33,
    "name": "赞妮",
    "star": 4,
    "element": null,
    "weapon": null,
    "item": null,
    "url": "1309607355563974656",
    "icon_url": "https://prod-alicdn-community.kurobbs.com/forum/2aa87c7266e248c1b5373675c603047920241122.png"
  }
]