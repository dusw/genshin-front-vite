export const zzzBangboo: any[] = [
  {
    "id": 1,
    "name": "阿崔巡查",
    "star": 5,
    "element": 4,
    "mhy_url": "679",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/09/10/76099754/b372bf18ed2b7ae9918ba4b712a56657_5026439487626757748.png"
  },
  {
    "id": 2,
    "name": "阿全",
    "star": 5,
    "element": 4,
    "mhy_url": "390",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/21/110289248/fa7c182438c2f230e9a01ddc0a317cba_1206610453014635860.png"
  },
  {
    "id": 3,
    "name": "艾米莉安",
    "star": 5,
    "element": 4,
    "mhy_url": "288",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/29/76099754/71357dfb8c243a1619d03d65b070fb1c_3126993636170599661.png"
  },
  {
    "id": 4,
    "name": "巴特勒",
    "star": 5,
    "element": 4,
    "mhy_url": "188",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/20/110289248/fb3914a1488716327e469929b28728ef_8767423714618566081.png"
  },
  {
    "id": 5,
    "name": "飚速布",
    "star": 5,
    "element": 1,
    "mhy_url": "287",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/28/72220812/c51c8bcd60b1b599cf3f091c7b67b3f7_250964181836901969.png"
  },
  {
    "id": 6,
    "name": "不良布",
    "star": 4,
    "element": 1,
    "mhy_url": "917",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/10/29/15559334/2d34667b314bcc6b7ef836f591fef7d4_9149936748865188683.png"
  },
  {
    "id": 7,
    "name": "插头布",
    "star": 5,
    "element": 0,
    "mhy_url": "481",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/06/30/4976467/3a0d0a23707bbfeb2d616bef1a4dea9a_1107561603706624622.png"
  },
  {
    "id": 8,
    "name": "赤红莫库斯",
    "star": 5,
    "element": 4,
    "mhy_url": "803",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/09/14/15559334/103d7c255a171b845ae5cb09627e7f2c_7796386990513709545.png"
  },
  {
    "id": 9,
    "name": "磁力布",
    "star": 4,
    "element": 4,
    "mhy_url": "472",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/06/19/110289248/2ed9c3365d582dbb398397880419fed4_4328365517977026268.png"
  },
  {
    "id": 10,
    "name": "电击布",
    "star": 4,
    "element": 0,
    "mhy_url": "90",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/21/76099754/95668b7af24000156f1321ca8dcb5c7f_7718842146577527146.png"
  },
  {
    "id": 11,
    "name": "恶魔布",
    "star": 4,
    "element": 3,
    "mhy_url": "76",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/20/76099754/0ed93968fadcbad2fb94eee71e7088c8_6022445324328656273.png"
  },
  {
    "id": 12,
    "name": "飞靶布",
    "star": 4,
    "element": 4,
    "mhy_url": "171",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/24/34600917/d412e63fd4c7d78b178601631f02f815_6723523575438064678.png"
  },
  {
    "id": 13,
    "name": "格斗布",
    "star": 4,
    "element": 1,
    "mhy_url": "998",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/12/13/158441880/850feec26efa8a7df6757c2ff1dcf50d_6971439166735407003.png"
  },
  {
    "id": 14,
    "name": "格列佛探员",
    "star": 5,
    "element": 0,
    "mhy_url": "999",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/12/13/158441880/a1daa7a41f6b4bbaeda796a472ba94f1_1534798435473169489.png"
  },
  {
    "id": 15,
    "name": "共鸣布",
    "star": 5,
    "element": 3,
    "mhy_url": "483",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/06/30/4976467/20660fe2f1fc198c07dbc673ff360491_1705764072868204497.png"
  },
  {
    "id": 16,
    "name": "果核布",
    "star": 4,
    "element": 99,
    "mhy_url": "159",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/24/76099754/3198c1a7bb1fecf219041a13ebcecfe0_563297318142808709.png"
  },
  {
    "id": 17,
    "name": "泪眼布",
    "star": 4,
    "element": 1,
    "mhy_url": "320",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/24/82500917/1796c4c3a6869affd86e33cf8a58c5ff_3628236273221888656.png"
  },
  {
    "id": 18,
    "name": "扑击布",
    "star": 4,
    "element": 4,
    "mhy_url": "319",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/24/82500917/d3222a7290c0e73b49d656cfcb9e4e96_157620243198274225.png"
  },
  {
    "id": 19,
    "name": "骑士布",
    "star": 4,
    "element": 3,
    "mhy_url": "918",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/10/29/15559334/f5ab6ee7cfacc84a72c178bfb9273caf_1620284387159602391.png"
  },
  {
    "id": 20,
    "name": "企鹅布",
    "star": 4,
    "element": 2,
    "mhy_url": "169",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/24/34600917/c29368317914b8b87297e73759d7c539_6689931196506648137.png"
  },
  {
    "id": 21,
    "name": "气压布",
    "star": 4,
    "element": 99,
    "mhy_url": "247",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/24/76099754/acd784197cbbf3971a377423272ddc7d_5275817419726187103.png"
  },
  {
    "id": 22,
    "name": "鲨牙布",
    "star": 5,
    "element": 2,
    "mhy_url": "109",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/21/110289248/ecc57b8b1f53099d872209f6ec1f7e3e_8968905895981709141.png"
  },
  {
    "id": 23,
    "name": "寻宝布",
    "star": 4,
    "element": 99,
    "mhy_url": "166",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/24/141353238/5658aeee3f8e2dbf9939f13c4efc9f88_652676394339983289.png"
  },
  {
    "id": 24,
    "name": "招财布",
    "star": 4,
    "element": 4,
    "mhy_url": "167",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/24/141353238/4a21245daa6c2ebb699609e5c21ac7b1_4999512287203748704.png"
  },
  {
    "id": 25,
    "name": "纸袋布",
    "star": 4,
    "element": 4,
    "mhy_url": "170",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/24/34600917/247cefe8b90f1c41d46b6dcc3270fd34_6221904965910532179.png"
  },
  {
    "id": 26,
    "name": "纸壳布",
    "star": 4,
    "element": 4,
    "mhy_url": "321",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/24/82500917/bbf85fb1d5b180fa46da0fff4a18f6eb_6098744804984530704.png"
  },
  {
    "id": 27,
    "name": "左轮布",
    "star": 5,
    "element": 4,
    "mhy_url": "482",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/06/30/4976467/4703576ebe3be6b50d05faf6ec0c3b54_4376277020509012212.png"
  }
]