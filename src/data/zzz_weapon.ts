export const zzzWeapon: any[] = [
  {
    "id": 1,
    "name": "奔袭獠牙",
    "weapon": 2,
    "star": 5,
    "item": 2,
    "mhy_url": "799",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/09/12/141353238/31b6008c9ea5f6a1f195c64bba5440a0_1820903942251897760.png"
  },
  {
    "id": 2,
    "name": "奔袭獠牙",
    "weapon": 2,
    "star": 5,
    "item": 2,
    "mhy_url": "799",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/09/12/141353238/31b6008c9ea5f6a1f195c64bba5440a0_1820903942251897760.png"
  },
  {
    "id": 3,
    "name": "残心青囊",
    "weapon": 1,
    "star": 5,
    "item": 1,
    "mhy_url": "991",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/12/09/160824341/9861761e3b5e1ff05158695f68ce6320_8061199014439521772.png"
  },
  {
    "id": 4,
    "name": "啜泣摇篮",
    "weapon": 4,
    "star": 5,
    "item": 4,
    "mhy_url": "218",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/27/11536806/ef3bdba96582ab932e00f488d9d84241_1480526923675603443.png"
  },
  {
    "id": 5,
    "name": "淬锋钳刺",
    "weapon": 0,
    "star": 5,
    "item": 0,
    "mhy_url": "760",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/08/27/160824341/15a02c67d9bcb3ba4b5727280adfb8fe_8180946295167199111.png"
  },
  {
    "id": 6,
    "name": "防暴者Ⅵ型",
    "weapon": 1,
    "star": 5,
    "item": 1,
    "mhy_url": "211",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/26/11536806/786a88680bf755b1c37354f661515b3f_1539116925377315728.png"
  },
  {
    "id": 7,
    "name": "钢铁肉垫",
    "weapon": 1,
    "star": 5,
    "item": 1,
    "mhy_url": "224",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/26/11536806/fbf66943a7dea171bbe1876abec75ab3_2359632281717116695.png"
  },
  {
    "id": 8,
    "name": "拘缚者",
    "weapon": 3,
    "star": 5,
    "item": 3,
    "mhy_url": "219",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/26/11536806/e635273d517c98e9724a7a11870c6fbb_6673349067631850236.png"
  },
  {
    "id": 9,
    "name": "硫磺石",
    "weapon": 1,
    "star": 5,
    "item": 1,
    "mhy_url": "223",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/26/11536806/2cd3aa09c2f818eadf4ce632c5f421fd_1967602675265095788.png"
  },
  {
    "id": 10,
    "name": "嵌合编译器",
    "weapon": 0,
    "star": 5,
    "item": 0,
    "mhy_url": "214",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/27/11536806/f870509a9a2979efe912c9d7785092b0_3413350005563459690.png"
  },
  {
    "id": 11,
    "name": "燃狱齿轮",
    "weapon": 3,
    "star": 5,
    "item": 3,
    "mhy_url": "220",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/26/11536806/b7b68f095152ab344e48b988f9906250_2480917059810186013.png"
  },
  {
    "id": 12,
    "name": "深海访客",
    "weapon": 1,
    "star": 5,
    "item": 1,
    "mhy_url": "225",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/26/11536806/b029d3bf8295e848bf4bb3cee7bee635_4869761072423699492.png"
  },
  {
    "id": 13,
    "name": "时流贤者",
    "weapon": 0,
    "star": 5,
    "item": 0,
    "mhy_url": "915",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/10/29/141353238/7ba42d92bd7e5b61fce77a99254aaa1d_4402724079782148317.png"
  },
  {
    "id": 14,
    "name": "霰落星殿",
    "weapon": 0,
    "star": 5,
    "item": 0,
    "mhy_url": "992",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/12/09/160824341/3ba720942094686d01e37dd510b7b029_7791594771544323139.png"
  },
  {
    "id": 15,
    "name": "焰心桂冠",
    "weapon": 3,
    "star": 5,
    "item": 3,
    "mhy_url": "951",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/11/29/76099754/a26c633d050e4360280942594764204c_4616316956671018004.png"
  },
  {
    "id": 16,
    "name": "玉壶青冰",
    "weapon": 3,
    "star": 5,
    "item": 3,
    "mhy_url": "678",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/07/31/82500917/6758b3dad33fa7e9f42bd512ee306199_4330887744653690767.png"
  },
  {
    "id": 17,
    "name": "灼心摇壶",
    "weapon": 0,
    "star": 5,
    "item": 0,
    "mhy_url": "841",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/10/08/158441880/a6467e15e97985d628b20eed5c1cdc55_7258114335694912644.png"
  },
  {
    "id": 18,
    "name": "比格气缸",
    "weapon": 2,
    "star": 4,
    "item": 2,
    "mhy_url": "143",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/22/82500917/3cc594d80af5a1661a0aae16298518e9_5212307620367843067.png"
  },
  {
    "id": 19,
    "name": "触电唇彩",
    "weapon": 0,
    "star": 4,
    "item": 0,
    "mhy_url": "210",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/27/11536806/d40322e5c28b18493b8893d34f0fc9ef_131091089874582327.png"
  },
  {
    "id": 20,
    "name": "春日融融",
    "weapon": 2,
    "star": 4,
    "item": 2,
    "mhy_url": "142",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/22/82500917/4910d45c5519a6e8c08b440105af486b_7973080142023073949.png"
  },
  {
    "id": 21,
    "name": "德玛拉电池Ⅱ型",
    "weapon": 3,
    "star": 4,
    "item": 3,
    "mhy_url": "98",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/21/11536806/28100408291b1bddadd6c416dcc2e5e4_966978839453439802.png"
  },
  {
    "id": 22,
    "name": "仿制星徽引擎",
    "weapon": 1,
    "star": 4,
    "item": 1,
    "mhy_url": "263",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/28/11536806/7a2092bf3e5d51bd8f0d425f21b4a5a2_1442414769992276141.png"
  },
  {
    "id": 23,
    "name": "贵重骨核",
    "weapon": 3,
    "star": 4,
    "item": 3,
    "mhy_url": "141",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/22/11536806/c97a046a5728d2e5eb420a1ca63e99d2_4259106039270095639.png"
  },
  {
    "id": 24,
    "name": "含羞恶面",
    "weapon": 4,
    "star": 4,
    "item": 4,
    "mhy_url": "215",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/27/11536806/9fed705aa18fdde455ded51e4f907096_7899311830170267448.png"
  },
  {
    "id": 25,
    "name": "好斗的阿炮",
    "weapon": 4,
    "star": 4,
    "item": 4,
    "mhy_url": "486",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/07/02/222203892/6de89d4ff56df24f73d020c6f056376a_5782990277416426429.png"
  },
  {
    "id": 26,
    "name": "轰鸣座驾",
    "weapon": 0,
    "star": 4,
    "item": 0,
    "mhy_url": "494",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/07/02/222203892/59d21937c3fed5bbd95981b7348905f2_2441560680559602996.png"
  },
  {
    "id": 27,
    "name": "加农转子",
    "weapon": 1,
    "star": 4,
    "item": 1,
    "mhy_url": "221",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/26/11536806/af4b2e547d6735da28ae46769b9a832e_5920935504776128925.png"
  },
  {
    "id": 28,
    "name": "家政员",
    "weapon": 1,
    "star": 4,
    "item": 1,
    "mhy_url": "264",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/28/11536806/c5e3277e051b5ad10ec568c8ed62e472_4200654280788559697.png"
  },
  {
    "id": 29,
    "name": "街头巨星",
    "weapon": 1,
    "star": 4,
    "item": 1,
    "mhy_url": "222",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/26/11536806/01d76f16b402716d76700acc35626517_3728798524435924794.png"
  },
  {
    "id": 30,
    "name": "聚宝箱",
    "weapon": 4,
    "star": 4,
    "item": 4,
    "mhy_url": "217",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/27/11536806/1e501eb6d186edb508944520bcca4aad_6474980928597087853.png"
  },
  {
    "id": 31,
    "name": "鎏金花信",
    "weapon": 1,
    "star": 4,
    "item": 1,
    "mhy_url": "744",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/08/01/15559334/5b63bca946b316faea7131615d2fcb6c_1822227116513416053.png"
  },
  {
    "id": 32,
    "name": "强音热望",
    "weapon": 1,
    "star": 4,
    "item": 1,
    "mhy_url": "995",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/12/14/34600917/a10b80da6d507c0432c9c1ce613a122e_1635217964961395708.png"
  },
  {
    "id": 33,
    "name": "人为刀俎",
    "weapon": 3,
    "star": 4,
    "item": 3,
    "mhy_url": "157",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/23/11536806/7928a573debd995598d48b0f3d5cb88b_7692397406690860586.png"
  },
  {
    "id": 34,
    "name": "时光切片",
    "weapon": 4,
    "star": 4,
    "item": 4,
    "mhy_url": "216",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/27/11536806/a945c3ba4d4b10a5be94ce9350f81268_6162591158214973464.png"
  },
  {
    "id": 35,
    "name": "双生泣星",
    "weapon": 0,
    "star": 4,
    "item": 0,
    "mhy_url": "212",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/27/11536806/198cf01b8c620882b72deab5a23c7cdd_3281667589262288413.png"
  },
  {
    "id": 36,
    "name": "兔能环",
    "weapon": 2,
    "star": 4,
    "item": 2,
    "mhy_url": "146",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/22/82500917/a750117496e57acbaa3dc4dd09f4101d_1934566378030591930.png"
  },
  {
    "id": 37,
    "name": "维序者-特化型",
    "weapon": 2,
    "star": 4,
    "item": 2,
    "mhy_url": "761",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/08/27/160824341/0e84a53f3f31d0611aba264f2138b468_7489558037732624988.png"
  },
  {
    "id": 38,
    "name": "逍遥游球",
    "weapon": 4,
    "star": 4,
    "item": 4,
    "mhy_url": "213",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/27/11536806/ebbceb9c733b032cc66fec9bab5b39cb_5028702273646752861.png"
  },
  {
    "id": 39,
    "name": "星徽引擎",
    "weapon": 1,
    "star": 4,
    "item": 1,
    "mhy_url": "156",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/23/11536806/4c71730c100ac556417529a8e4598d36_2327866382544117474.png"
  },
  {
    "id": 40,
    "name": "旋钻机-赤轴",
    "weapon": 1,
    "star": 4,
    "item": 1,
    "mhy_url": "265",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/28/11536806/51cfdbcf65d8bca2826acec5ba5bd34c_3722412418301406226.png"
  },
  {
    "id": 41,
    "name": "雨林饕客",
    "weapon": 0,
    "star": 4,
    "item": 0,
    "mhy_url": "154",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/23/11536806/859457119870b43e2e323651436b82e2_7772525266668421289.png"
  },
  {
    "id": 42,
    "name": "正版变身器",
    "weapon": 2,
    "star": 4,
    "item": 2,
    "mhy_url": "139",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/22/82500917/09d6a527710604c64683c74ce0185cf9_3635163469335796053.png"
  },
  {
    "id": 43,
    "name": "左轮转子",
    "weapon": 3,
    "star": 4,
    "item": 3,
    "mhy_url": "155",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/23/11536806/864547e11bff3e3c0a2eb77904e1a506_6842086161355620091.png"
  },
  {
    "id": 44,
    "name": "「残响」-Ⅰ型",
    "weapon": 4,
    "star": 3,
    "item": 4,
    "mhy_url": "128",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/23/34600917/253bf13e8132045687243a7d28afa23f_2427862443496393199.png"
  },
  {
    "id": 45,
    "name": "「残响」-Ⅱ型",
    "weapon": 4,
    "star": 3,
    "item": 4,
    "mhy_url": "129",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/23/34600917/a7cf76336e9afdd9d38f9d652d516d94_4327705131427584663.png"
  },
  {
    "id": 46,
    "name": "「残响」-Ⅲ型",
    "weapon": 4,
    "star": 3,
    "item": 4,
    "mhy_url": "93",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/21/34600917/059a75f3681deb2e9c78cc9835dc82b8_8996856758612183619.png"
  },
  {
    "id": 47,
    "name": "「电磁暴」-贰式",
    "weapon": 0,
    "star": 3,
    "item": 0,
    "mhy_url": "131",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/23/34600917/a18b73a46dd4d1a1ac44b316b6ad03c9_792685800358740562.png"
  },
  {
    "id": 48,
    "name": "「电磁暴」-叁式",
    "weapon": 0,
    "star": 3,
    "item": 0,
    "mhy_url": "130",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/23/34600917/c9c75ae6e211e91707bed1335b6c0091_4607145853356936887.png"
  },
  {
    "id": 49,
    "name": "「电磁暴」-壹式",
    "weapon": 0,
    "star": 3,
    "item": 0,
    "mhy_url": "132",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/23/34600917/21d0c3aa5c78d4b856edbb9d17159078_11309337161817092.png"
  },
  {
    "id": 50,
    "name": "「恒等式」-本格",
    "weapon": 2,
    "star": 3,
    "item": 2,
    "mhy_url": "126",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/22/34600917/b2518a5c40e0afebe91b2095726ad09b_4654997948728681847.png"
  },
  {
    "id": 51,
    "name": "「恒等式」-变格",
    "weapon": 2,
    "star": 3,
    "item": 2,
    "mhy_url": "127",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/22/34600917/ab8ffe09eb3a17509712aeeb08c81907_5621522584096526732.png"
  },
  {
    "id": 52,
    "name": "「湍流」-铳型",
    "weapon": 3,
    "star": 3,
    "item": 3,
    "mhy_url": "116",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/22/82500917/678e013c400c296a78738177b6a1a5a8_5631636824441755623.png"
  },
  {
    "id": 53,
    "name": "「湍流」-斧型",
    "weapon": 3,
    "star": 3,
    "item": 3,
    "mhy_url": "133",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/23/34600917/89d2801f10986bbe99af9a42e4042810_1308787568708134435.png"
  },
  {
    "id": 54,
    "name": "「湍流」-矢型",
    "weapon": 3,
    "star": 3,
    "item": 3,
    "mhy_url": "119",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/22/82500917/95b56323f1157a73903b58b0d0618d82_1657980502405234177.png"
  },
  {
    "id": 55,
    "name": "「月相」-晦",
    "weapon": 1,
    "star": 3,
    "item": 1,
    "mhy_url": "114",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/22/82500917/941a1dbc1f0f5ea154f89a977688f56f_7644642860191957526.png"
  },
  {
    "id": 56,
    "name": "「月相」-朔",
    "weapon": 1,
    "star": 3,
    "item": 1,
    "mhy_url": "117",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/22/82500917/02ec27514360d65720b1b8fb011fdd65_563337634939069981.png"
  },
  {
    "id": 57,
    "name": "「月相」-望",
    "weapon": 1,
    "star": 3,
    "item": 1,
    "mhy_url": "118",
    "icon_url": "https://act-upload.mihoyo.com/nap-obc-indep/2024/05/22/82500917/62251019ac4ef58fd4c279f83524cd84_4183639709296529508.png"
  }
]
